ifeq ($(TARGET_GAPPS_ARCH),)
$(error "GAPPS: TARGET_GAPPS_ARCH is undefined")
endif

ifneq ($(TARGET_GAPPS_ARCH),arm64)
$(error "GAPPS: Only arm64 are allowed")
endif

$(call inherit-product, vendor/gapps/common-blobs.mk)

# Overlays
#DEVICE_PACKAGE_OVERLAYS += $(LOCAL_PATH)/overlay

# Allow overlays to be excluded from enforcing RRO
#PRODUCT_ENFORCE_RRO_EXCLUDED_OVERLAYS += $(LOCAL_PATH)/overlay

# framework
PRODUCT_PACKAGES += \
    com.google.android.maps

# dialer support
PRODUCT_PACKAGES += \
    com.google.android.dialer.support

# messages
PRODUCT_PACKAGES += \
    PrebuiltBugle

# System app
PRODUCT_PACKAGES += \
    GoogleExtShared

# System priv-app
PRODUCT_PACKAGES += \
    GoogleExtServices

# Product app
PRODUCT_PACKAGES += \
    CalculatorGooglePrebuilt \
    CalendarGooglePrebuilt \
    GoogleCalendarSyncAdapter \
    GoogleContacts \
    GoogleContactsSyncAdapter \
    GoogleLocationHistory \
    MarkupGoogle \
    PrebuiltDeskClockGoogle \
    PrebuiltGmail

# Product priv-app
PRODUCT_PACKAGES += \
    AndroidMigratePrebuilt \
    CarrierSetup \
    ConfigUpdater \
    GmsCoreSetupPrebuilt \
    GoogleBackupTransport \
    GoogleRestore \
    GoogleServicesFramework \
    Phonesky \
    SetupWizard \
    PrebuiltGmsCoreQt \
    WellbeingPrebuilt

# Dialer
#PRODUCT_PACKAGES += \
#    GoogleDialer
